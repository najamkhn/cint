#!/usr/bin/env ts-node
/// <reference path="./lib/reader.d.ts" />
import * as reader from './lib/reader';

interface Point {
    x: number;
    y: number;
};

class Robot {
    coords: Point;
    movements: number[] = [];

    constructor(coords: Point) {
        this.setCoords(coords);
        this.track();
    }

    setCoords(coords: Point): void {
        this.coords = coords;
    }

    // Move will execute the number of atomic steps that the robot is going to take. So the
    // function will add the direction and update the tracker as well for robot location.
    move(direction: string, steps: number): void {
        for (let x = 0; x < steps; x++) {
            this.step(direction);
            this.track();
        }
    }

    // All steps are atomic (i.e, there's no jumping) therfore there'll always
    // be an increment in whichever direction robot is assigned to move.
    step(direction: string): void {
        switch (direction) {
            case 'E':
                this.coords.x = this.coords.x + 1;
                break;
            case 'W':
                this.coords.x = this.coords.x - 1;
                break;
            case 'N':
                this.coords.y = this.coords.y + 1;
                break;
            case 'S':
                this.coords.y = this.coords.y - 1;
                break;
            default: break;
        }
    }

    // This is going to record the movement of robot in a 2x2 matrix (i.e, x and y)
    // and store the calculated index.
    track(): void {
        const index: number = (2 * this.coords.x) + +this.coords.y;
        if (this.movements.indexOf(index) < 0) {
            this.movements.push(index);
        }
    }

    getUniqueLocations(): number {
        return this.movements.length;
    }
};

const main = async () => {
    // Receiving parsed input from the file stream
    const data: any = await reader.Reader();
    const moveCount: any = data[0];

    const initialPosition = <Point>reader.parseInputs(data[1]);
    const robot = new Robot(initialPosition);

    for (let i = 0; i < moveCount; i++) {
        const [dir, steps] = reader.parseDirections(data[i + 2]);
        robot.move(dir, steps);
    }

    console.log('=> Cleaned: ' + robot.getUniqueLocations());
};

main();
