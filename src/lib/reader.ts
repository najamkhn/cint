export function parseDirections(str: string) : any[] {
    const input = str.split(/[\s,]+/);
    return [...input];
}

export function parseInputs(str: string): object {
    const input = str.split(/[\s,]+/);
    return {
        x: input[0], 
        y: input[1]
    };
}

export function Reader(): object {
    return new Promise<any[]>((resolve, reject) => {
        let input: any = '';

        process.stdin.on('data', _ => {
            input += _;
        });

        process.stdin.on('end', _ => {
            input = input.split('\n').map(x => x.trim());
            resolve(input);
        });
    });
};