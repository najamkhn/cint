Cint Test
===

This is the implementation of Cint Programming Challenge using TypeScript. I've solved it using classes for convenience as Point (x, y) can be better expressed with an interface/classes. 

To install dependencies, please run `npm install` and then to run it with the input data please run the command below:

```
cat input.txt | $(npm bin)/ts-node src/main.ts
```

